import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

// Import Icons 'npm isntall --save react-native-vector-icons'
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class videoItems extends React.Component {
    render() {
        // Pass the dummy data from data.json
        let video = this.props.vids;

        // alert(video.id); - Check if the data passed successfully
        return(
            <View style={styles.container}>
                <Image source={{url: video.snippet.thumbnails.medium.url}} style={{height: 200}} />
                <View style={styles.descContainer}>
                    <Image source={{uri:'https://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg'}} style={styles.channelProfPict} />

                    <View style={styles.videoDetails}>
                        <Text style={styles.videoTitle}>{video.snippet.title}</Text>

                        <Text style={styles.videoStats}>{video.snippet.channelTitle} · {nFormatter(video.statistics.viewCount)} views · 3 months ago</Text>
                    </View>

                    <TouchableOpacity>
                        <Icon name='more-vert' size={24} color={'#999999'} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        // backgroundColor: 'red'
    },

    descContainer: {
        flexDirection: 'row',
        paddingVertical: 15
    },

    channelProfPict: {
        width: 50,
        height: 50,
        borderRadius: 100
    },

    videoDetails: {
        paddingHorizontal: 16,
        flex: 1,
    },

    videoTitles: {
        color: '#3c3c3c',
    },

    videoStats: {
        color: '#999999',
        flex: 1,
        fontSize: 12,
        marginTop: 2
    }
});

// Format angka kalau angka > 1000 = 1K
function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
      }
    }
}