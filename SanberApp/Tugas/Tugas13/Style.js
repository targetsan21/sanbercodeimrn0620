'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        overflow: 'hidden',
    },

    logoStyle: {
        height: 30, 
        width: 98,
        resizeMode: 'contain', 
        marginTop: 16
    },

    welcomeBanner: {
        borderRadius: 8,
        overflow: 'hidden',
        marginTop: 16,
        width: '100%',
        height: 111,
        justifyContent: 'center',
        // borderColor: 'red',
        // borderWidth: 2
    },

    bannerbg: {
        width: '100%',
        resizeMode: 'contain'
    },

    bannerText: {
        position: 'absolute',
        marginLeft: 16
    },

    heroText: {
        fontSize: 32,
        fontWeight: 'bold',
        color: '#fff',
    },

    signUpButton: {
        borderRadius: 8,
        marginVertical: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 12
    },

    icon: {
        width: 24,
        height: 24,
        marginRight: 8
    },

    createAccSect: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 16
    },

    line: {
        width: '16%',
        borderWidth: 1,
        borderColor: '#DEDEDE'
    },

    // Text Style
    h1Reg: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#545454'
    },

    pReg: {
        fontSize: 16,
        color: '#545454'
    },

    pBold: {
        fontWeight: 'bold',
        fontSize: 16
    },

    pMed: {
        fontSize: 16,
        fontWeight: '600',
        color: '#545454'
    },

    pLink: {
        color: '#56CCF2',
        textDecorationLine: 'underline'
    },

    label: {
        color: '#545454',
        fontSize: 12,
    },

    // Input Style
    inputCont: {
        marginTop: 16
    },

    inputBox: {
        backgroundColor: '#FFF6ED',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 16,
        borderRadius: 8,
        marginTop: 8
    },

    input: {
        fontSize: 16,
        flex: 1,
        paddingHorizontal: 12
    },

    submitButton: {
        marginTop: 16,
        height: 56,
        borderRadius: 8,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: 'red'
    },

    // About Me Style
    profileBanner: {
        height: 126,
        width: '100%',
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12
    },

    pp: {
        height: '100%',
        resizeMode:'contain'
    },

    profilePictCont: {
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        marginTop: -60
        // backgroundColor: 'red'
    },

    profilePict: {
        width: 112,
        height: 112,
        borderRadius: 100,
        overflow: 'hidden',
        borderWidth: 8,
        borderColor: '#fff'
        // backgroundColor: 'red'
    },

    profileData: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 4
    },

    proTagCont: {
        flexDirection: 'row',
        marginTop: 12
    },

    proTag: {
        marginHorizontal: 4,
        backgroundColor: '#FFF6ED',
        paddingHorizontal: 12,
        paddingVertical: 8,
        borderRadius: 4
    },

    location: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8
    },

    quote: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF6ED',
        marginTop: 16,
        borderRadius: 24,
        paddingHorizontal: 32,
        paddingVertical: 24
    },

    socMed: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 12
    },

    // Skill Screen
    skillHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 12
    },

    ppSmall: {
        height: 48,
        width: 48,
        borderRadius: 100,
        overflow: 'hidden',
        marginLeft: 16
    },

    profDetailSmall: {
        marginLeft: 8
    },

    skills: {
        alignItems: 'center',
        marginTop: 32
    },

    skill: {
        flexDirection: 'row',
        marginTop: 16,
        marginBottom: 32
    },

    skillData: {
        flexDirection: 'row',
        marginHorizontal: 8
    }
})