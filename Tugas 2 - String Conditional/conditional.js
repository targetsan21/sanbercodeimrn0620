// Soal 1 - If-Else
// input
var nama = 123;
var peran = 'GuArd';

var namaStr = String(nama); /* bikin nama jadi string */
var peranStr = String(peran); /* bikin peran jadi string */

var peranLoCase = peranStr.toLowerCase(); /* lower case peran */
var peranCap = peranLoCase.charAt(0).toUpperCase() + peranLoCase.slice(1) /* upper case huruf pertama peran */

// cek Nama
if (namaStr == '') {
    console.log('Nama harus diisi!');
    return;
};

// cek Peran ada apa nggak
if (peranLoCase === '') {
    console.log('Halo ' + namaStr + ', pilih peranmu untuk memulai game.');
    return;
} 

// welcoming dan detail peran
if (peranLoCase === 'penyihir' || peranLoCase === 'guard' || peranLoCase === 'werewolf') {
    console.log('Selamat datang di dunia Werewolf, ' + nama + '.');
    if (peranLoCase === 'penyihir') {
        console.log('Halo ' + peranCap + ' ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!');
    } else if (peranLoCase === 'guard') {
        console.log('Halo ' + peranCap + ' ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.');
    } else if (peranLoCase === 'werewolf') {
        console.log('Halo ' + peranCap + ' ' + nama + ', kamu akan memakan mangsa setiap malam!')
    }
} else {
    console.log('Coba periksa lagi peranmu! Peran yang tersedia adalah "Penyihir", "Guard", dan "Werewolf"')
    return;
}

// Tugas 2 - Switch Case
// Input
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

// cek apakah data input itu integer tanpa desimal
if (Number.isInteger(hari) == false || Number.isInteger(bulan) == false || Number.isInteger(tahun) == false) {
    console.log('Kamu hanya bisa memasukan angka, dan tanpa desimal');
    return;
};

// memproses data inputan
if (hari < 1 || hari > 31) {
    console.log('Hari hanya boleh diisi angka 1 - 31');
    return;
} else if (bulan < 1 || bulan > 12) {
    console.log('Bulan hanya boleh diisi angka 1 - 12');
    return;
} else if (tahun < 1900 || tahun > 2200) {
    console.log('Tahun hanya boleh diisi angka 1900 - 2200');
    return;
} else {
    switch(bulan) {
        case 1 :
            console.log(hari + ' Januari ' + tahun);
            break;
        case 2 :
            console.log(hari + ' Februari ' + tahun);
            break;
        case 3 :
            console.log(hari + ' Maret ' + tahun);
            break;
        case 4 : 
            console.log(hari + ' April ' + tahun);
            break;
        case 5 :
            console.log(hari + ' Mei ' + tahun);
            break;
        case 6 :
            console.log(hari + ' Juni ' + tahun);
            break;
        case 7 :
            console.log(hari + ' Juli ' + tahun);
            break;
        case 8 :
            console.log(hari + ' Agustus ' + tahun);
            break;
        case 9 :
            console.log(hari + ' September ' + tahun);
            break;
        case 10 :
            console.log(hari + ' Oktober ' + tahun);
            break;
        case 11 :
            console.log(hari + ' November ' + tahun);
            break;
        case 12 :
            console.log(hari + ' Desember ' + tahun);
            break;
    };
    return;
}