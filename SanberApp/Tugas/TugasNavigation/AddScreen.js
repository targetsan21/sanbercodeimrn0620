import React, { Component } from 'react';
import { View, Text } from 'react-native';

// Import Style
import Style from '../Tugas13/Style';

export default class App extends Component {
    render() {
        return(
            <View style={[Style.container, {justifyContent: 'center', alignItems: "center"}]}>
                <Text>Halaman Tambah</Text>
            </View>
        )
    }
}