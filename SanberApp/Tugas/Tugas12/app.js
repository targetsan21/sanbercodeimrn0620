import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, FlatList } from 'react-native';

// Import Icons 'npm isntall --save react-native-vector-icons'
import Icon from 'react-native-vector-icons/MaterialIcons';

// Import Video Items
import VideoItem from './components/videoitems';

// Import Data
import data from './data.json';

export default class app extends React.Component {
    render() {
        // alert(data.item.length); - Check if the data imported successfully > Alert 5
        return(
            <View style={styles.container}>
                <View style={{height: 24, backgroundColor: '#000000'}}></View>

                <View style={styles.navBar}>
                    <Image source={require('./images/logo.png')} style={{width: 98, height: 22}} />
                    <View style={styles.rightNav}>
                        <TouchableOpacity>
                            <Icon style={styles.navItem} name='search' size={25} />
                        </TouchableOpacity>

                        <TouchableOpacity>                       
                            <Icon style={styles.navItem} name='account-circle' size={25} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.body}>
                    {/* <VideoItem vids={data.items[0]} /> */}

                    <FlatList
                        data={data.items}
                        renderItem={(video)=><VideoItem vids={video.item} />}
                        keyExtractor={(item)=>item.id}
                        ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                    />
                </View>

                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name='home' size={25} color='#3c3c3c' />
                        <Text style={styles.tabText}>Home</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name='whatshot' size={25} color='#3c3c3c' />
                        <Text style={styles.tabText}>Trending</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name='subscriptions' size={25} color='#3c3c3c' />
                        <Text style={styles.tabText}>Subscriptions</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name='folder' size={25} color='#3c3c3c' />
                        <Text style={styles.tabText}>Library</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
    },

    navBar: {
        height: 56,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dedede',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16
    },

    rightNav: {
        flexDirection: 'row',
    },

    navItem: {
        marginLeft: 16,
    },

    body: {
        flex: 1
    },

    tabBar: {
        height: 56,
        borderTopWidth: 1,
        borderTopColor: '#dedede',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: 16
    },

    tabItem: {
        alignItems: 'center',
    },

    tabText: {
        fontSize: 11,
        marginTop: 4
    },
});