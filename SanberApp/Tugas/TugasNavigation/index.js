// React Import
import React from 'react';

// React Nav Import
import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

// Screens Import
import SignUp from '../Tugas13/LoginScreen';
import AboutScreen from '../Tugas13/AboutScreen';
import SkillScreen from '../Tugas14/SkillScreen';
import AddScreen from './AddScreen';
import ProjectScreen from './ProjectScreen'

const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const AboutTabScreen = () => {
    return(
        <Tabs.Navigator>
            <Tabs.Screen name='About' component={ AboutScreen } />
            <Tabs.Screen name='Skills' component={ SkillScreen } />
            <Tabs.Screen name='Project' component={ ProjectScreen } />
            <Tabs.Screen name='Add' component={ AddScreen } />
        </Tabs.Navigator>
    )
}

export default()  => {
    return (
        <NavigationContainer>
            <Drawer.Navigator>
                <Drawer.Screen name='Sign Up' component={ SignUp } />
                <Drawer.Screen name='About Creator' component={ AboutTabScreen } />
            </Drawer.Navigator>
        </NavigationContainer>
    )
}