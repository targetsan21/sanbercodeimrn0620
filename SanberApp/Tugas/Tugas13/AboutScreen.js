import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Style from './Style' // Import Style

export default class App extends Component {
    render() {
        return(
            <View style={[Style.container, {marginTop: 0}]}>
                <ScrollView>
                    {/* Profile Banner */}
                    <View style={Style.profileBanner}>
                        <Image source={require('./Image/bubble-bg.png')} style={Style.pp} />
                    </View>

                    {/* Overflow Profile Pict */}
                    <View style={Style.profilePictCont}>
                        <View style={Style.profilePict}>
                            <Image source={require('./Image/pp.png')} style={{resizeMode: 'contain', height: 100, width: 100}} />
                        </View>
                    </View>

                    <View style={{paddingHorizontal: 24, marginBottom: 24, overflow: 'visible'}}>
                        {/* Profile Data */}
                        <View style={Style.profileData}>
                            {/* Name */}
                            <Text style={Style.h1Reg}>Target Santana</Text>

                            {/* Pro Tags */}
                            <View style={Style.proTagCont}>
                                <View style={Style.proTag}>
                                    <Text style={[Style.label, {color: '#FFC58B'}]}>Graphic Designer</Text>
                                </View>

                                <View style={Style.proTag}>
                                    <Text style={[Style.label, {color: '#FFC58B'}]}>UI Designer</Text>
                                </View>
                            </View>

                            {/* Location */}
                            <View style={Style.location}>
                                <Icon name='map-marker-alt' size={16} color='#E66FBF' />

                                <Text style={[Style.label, {color: '#E66FBF', marginLeft: 8}]}>Jakarta, Indonesia</Text>
                            </View>

                            {/* Quote */}
                            <View style={Style.quote}>
                                <Icon name='quote-left' size={32} color='#FFC58B' />

                                <Text style={[Style.pReg, {marginTop: 8, textAlign: 'center', color: '#757575'}]}>I'm a designer, a problem solver, a fast learner, agile, and a person who always have the ambition to innovate.</Text>
                            </View>

                            {/* Social Media */}
                            <View style={{marginVertical: 8}}>
                                <View style={Style.socMed}>
                                    <Icon name='behance' size={24} color='#545454' />
                                    <Text style={[Style.pReg, {marginLeft: 8}]}>behane.net/targetsantana</Text>
                                </View>

                                <View style={Style.socMed}>
                                    <Icon name='linkedin-in' size={24} color='#545454' />
                                    <Text style={[Style.pReg, {marginLeft: 8}]}>in/targetsantana</Text>
                                </View>

                                <View style={Style.socMed}>
                                    <Icon name='instagram' size={24} color='#545454' />
                                    <Text style={[Style.pReg, {marginLeft: 8}]}>@target_santana</Text>
                                </View>

                                <View style={Style.socMed}>
                                    <Icon name='gitlab' size={24} color='#545454' />
                                    <Text style={[Style.pReg, {marginLeft: 8}]}>@targetsan21</Text>
                                </View>
                            </View>

                            {/* Submit Button */}
                            <TouchableOpacity style={Style.submitButton}>
                                <LinearGradient 
                                    colors = {['#FFCA41', '#D941FF']}
                                    start = {[-1, 4]}
                                    end = {[1.8, 1]}
                                    style = {{width: '100%', height: 56, position: 'absolute'}}
                                />

                                <Text style={[Style.pMed, {color: 'white'}]}>See My Skill</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}