var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
let myTime = 10000;

function bookReadTime(num) {
    if(num === books.length) {
        return 0;
    } else {
        readBooksPromise(myTime, books[num])
            .then(function(fulfilled) {
                myTime = fulfilled;
                bookReadTime(num+1);
            })
            .catch(function(rejected) {
                return rejected;
            })
    }
}
bookReadTime(0)