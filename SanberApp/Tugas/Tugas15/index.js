import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { SignIn, CreateAccount, Profile, Home, Search, Search2, Details } from './Screen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer'

const AuthStack = createStackNavigator();

const Tabs = createBottomTabNavigator();

const Drawer = createDrawerNavigator();

const HomeStackScreen = () => {
  return(
    <AuthStack.Navigator>
      <AuthStack.Screen name='Home' component={ Home } options={{ title: 'Home' }} />
      <AuthStack.Screen name='Details' component={ Details } options={ ({ route }) => ({ title: route.params.name }) } />
    </AuthStack.Navigator>
  )
}

const ProfileStackScreen = () => {
  return(
    <AuthStack.Navigator>
      <AuthStack.Screen name='Profile' component={ Profile } options={{ title: 'Profile' }} />
    </AuthStack.Navigator>
  )
}

const SearchStackScreen = () => {
  return(
    <AuthStack.Navigator>
      <AuthStack.Screen name='Search' component={ Search } options={{ title: 'Search' }} />
      <AuthStack.Screen name='Search2' component={ Search2 } options={{ title: 'Search 2' }} />
    </AuthStack.Navigator>
  )
}

const TabsScreen = () => {
  return(
    <Tabs.Navigator>
      <Tabs.Screen name='Home' component={ HomeStackScreen } />
      <Tabs.Screen name='Search' component={ SearchStackScreen } />
      {/* <Tabs.Screen name='Profile' component ={ ProfileStackScreen } /> */}
    </Tabs.Navigator>
  )
}

export default () => {
  return(
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name='Home' component={ TabsScreen } />
        <Drawer.Screen name='My Account' component={ ProfileStackScreen } /> 
      </Drawer.Navigator>

      {/* <Tabs.Navigator>
        <Tabs.Screen name='Home' component={ HomeStackScreen } />
        <Tabs.Screen name='Search' component={ SearchStackScreen } />
        <Tabs.Screen name='Profile' component={ ProfileStackScreen } />
      </Tabs.Navigator> */}

      {/* <AuthStack.Navigator>
        <AuthStack.Screen name='SignIn' component={ SignIn } options={{ title: 'Sign In' }} />
        <AuthStack.Screen name='CreateAccount' component={ CreateAccount } options={{ title: 'Create Account' }} />
      </AuthStack.Navigator> */}
    </NavigationContainer>
  )
}