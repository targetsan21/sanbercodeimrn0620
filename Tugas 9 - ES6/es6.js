// Soal 1
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
golden()

// ES6
const goldenFunction = () => {
    return 'This is golden!!'
}

console.log(goldenFunction())

// ===========

// Soal 2
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName() 

// ES6
const literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        get fullName() {
            return `${firstName} ${lastName}`
        }
    }
}

console.log(literal('William', 'Imoh').fullName)

// Soal 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// ES6
const { firstName, lastName, destination, occupation, spell } = newObject

// Manggil
console.log(firstName, lastName, destination, occupation)

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

// ES6
let combine = [...west, ...east]

// Driver Code
console.log(combined)
console.log(combine)

// Soal 5
const planet = "earth"
const view = "glass"

var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'

const after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 
console.log(after)