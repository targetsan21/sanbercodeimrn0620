// Soal 1
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var joinWord1 = word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh; /* Jawaban Pertama */

var joinWord2 = word.concat(' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh); /* Jawaban Kedua */

console.log(joinWord1);
console.log(joinWord2);

// Soal 2
var sentence = "I am going to be React Native Developer"; 

var first = sentence[0];
var second = sentence.substr(sentence.indexOf('am'), 2);
var third = sentence.substr(sentence.indexOf('going'), 5);
var fourth = sentence.substr(sentence.indexOf('to'), 2);
var fifth = sentence.substr(sentence.indexOf('be'), 2);
var sixth = sentence.substr(sentence.indexOf('React'), 5);
var seventh = sentence.substr(sentence.indexOf('Native'), 6);
var eighth = sentence.substr(sentence.indexOf('Developer'), 9);

console.log('First Word: ' + first); 
console.log('Second Word: ' + second); 
console.log('Third Word: ' + third); 
console.log('Fourth Word: ' + fourth); 
console.log('Fifth Word: ' + fifth); 
console.log('Sixth Word: ' + sixth); 
console.log('Seventh Word: ' + seventh); 
console.log('Eighth Word: ' + eighth)

// Soal 3
var sentence2 = 'wow JavaScript is so cool';

var first2 = sentence2.substr(sentence2.indexOf('wow'), 3);
var second2 = sentence2.substr(sentence2.indexOf('JavaScript'), 10);
var third2 = sentence2.substr(sentence2.indexOf('is'), 2);
var fourth2 = sentence2.substr(sentence2.indexOf('so'), 2);
var fifth2 = sentence2.substr(sentence2.indexOf('cool'), 4);

console.log('First Word: ' + first2); 
console.log('Second Word: ' + second2); 
console.log('Third Word: ' + third2); 
console.log('Fourth Word: ' + fourth2); 
console.log('Fifth Word: ' + fifth2);

// Soal 4
var firstLength = first2.length;
var secondLength = second2.length;
var thirdLength = third2.length;
var fourthLength = fourth2.length;
var fifthLength = fifth2.length;

console.log('First Word: ' + first2 + ', with length: ' + firstLength); 
console.log('Second Word: ' + second2 + ', with length: ' + secondLength); 
console.log('Third Word: ' + third2 + ', with length: ' + thirdLength); 
console.log('Fourth Word: ' + fourth2 + ', with length: ' + fourthLength); 
console.log('Fifth Word: ' + fifth2 + ', with length: ' + fifthLength);