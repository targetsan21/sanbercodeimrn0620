// Soal 1
    // Release 0

class Animal {
    constructor(nama) {
        this.animalName = nama;
        this.animalLegs = 4;
        this.animalBlood = false;
    }
    get name() {
        return this.animalName;
    }
    get legs() {
        return this.animalLegs;
    }
    get cold_blooded() {
        return this.animalBlood;
    }
}
 
var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

    // Release 1
class Ape extends Animal{
    constructor(namaKera) {
        super(namaKera);
    }
    yell() {
        console.log('Auooo')
    }
}

var sungokong = new Ape('Kera Sakti');
sungokong.yell();

class Frog extends Animal {
    constructor(namaKatak) {
        super(namaKatak);
    }
    jump () {
        console.log('hop hop')
    }
}

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

// Soal 2
class Clock {
    constructor({ template }) {
        this.template = template;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (this.hours < 10) hours = '0' + hours

        let mins = date.getMinutes();
        if (this.mins < 10) mins = '0' + mins

        let secs = date.getSeconds()
        if (this.secs < 10) secs = '0' + secs

        let output = this.template.replace('h', hours).replace('m', mins).replace('s', secs);

        console.log(output)
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer - setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  