import React, { Component } from 'react';
import { View, Text, Image, SafeAreaView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Style from '../Tugas13/Style';

export default class App extends Component {
    render() {
        return(
            <View style={Style.container}>
                <SafeAreaView style={{marginHorizontal: 24}}>
                    {/* Header */}
                    <View style={Style.skillHeader}>
                        {/* Back Icon */}
                        <Icon name='chevron-left' size={24} color='#545454' />

                        {/* ProfilePict */}
                        <View style={Style.ppSmall}>
                            <Image source={require('../Tugas13/Image/pp.png')} style={{width: '100%', height: '100%', resizeMode: 'contain'}} />
                        </View>

                        {/* Location */}
                        <View style={Style.profDetailSmall}>
                            <Text style={Style.pMed}>Target Santana</Text>

                            <View style={[Style.location, {marginTop: 0}]}>
                                <Icon name='map-marker-alt' size={16} color='#E66FBF' />

                                <Text style={[Style.label, {color: '#E66FBF', marginLeft: 8}]}>Jakarta, Indonesia</Text>
                            </View>
                        </View>
                    </View>

                    {/* Quote */}
                    <View style={Style.quote}>
                        <Icon name='quote-left' size={32} color='#FFC58B' />

                        <Text style={[Style.pReg, {textAlign: 'center', color: '#757575'}]}>Skilled in design and its various softwares such as Adobe Illustrator, Photoshop, Premiere Pro, After Effect, XD, Figma, Studio, etc.</Text>
                    </View>

                    {/* Skills */}
                    <View style={Style.skills}>
                        {/* Programming Skill */}
                        <Text style={Style.pMed}>Programming Skill</Text>
                        
                        {/* JS */}
                        <View style={Style.skill}>
                            <View style={Style.skillData}>
                                <Image source={require('../Tugas13/Image/js.png')} />
                                
                                <View style={{marginLeft: 8}}>
                                    <Text style={Style.pReg}>JavaScript</Text>
                                    <Text style={[Style.label, {color: '#E66FBF'}]}>Intermediate</Text>
                                </View>
                            </View>
                        </View>

                        {/* Framework */}
                        <Text style={Style.pMed}>Framework</Text>
                        
                        {/* RN */}
                        <View style={Style.skill}>
                            <View style={Style.skillData}>
                                <Image source={require('../Tugas13/Image/rn.png')} />
                                
                                <View style={{marginLeft: 8}}>
                                    <Text style={Style.pReg}>React Native</Text>
                                    <Text style={[Style.label, {color: '#E66FBF'}]}>Intermediate</Text>
                                </View>
                            </View>
                        </View>

                        {/* Technologies */}
                        <Text style={Style.pMed}>Technologies</Text>
                        
                        {/* GitLab | GitHub */}
                        <View style={Style.skill}>
                            <View style={Style.skillData}>
                                <Image source={require('../Tugas13/Image/gitlab.png')} />
                                
                                <View style={{marginLeft: 8}}>
                                    <Text style={Style.pReg}>GitLab</Text>
                                    <Text style={[Style.label, {color: '#FFC58B'}]}>Beginner</Text>
                                </View>
                            </View>

                            <View style={Style.skillData}>
                                <Image source={require('../Tugas13/Image/github.png')} />
                                
                                <View style={{marginLeft: 8}}>
                                    <Text style={Style.pReg}>GitHub</Text>
                                    <Text style={[Style.label, {color: '#FFC58B'}]}>Beginner</Text>
                                </View>
                            </View>
                        </View>

                        {/* Design Softwares */}
                        <Text style={Style.pMed}>Design Softwares</Text>
                        
                        {/* Figma | XD | Studio */}
                        <View style={Style.skill}>
                            <View style={Style.skillData}>
                                <Image source={require('../Tugas13/Image/figma.png')} />
                                
                                <View style={{marginLeft: 8}}>
                                    <Text style={Style.pReg}>Figma</Text>
                                    <Text style={[Style.label, {color: '#9B51E0'}]}>Advanced</Text>
                                </View>
                            </View>

                            <View style={Style.skillData}>
                                <Image source={require('../Tugas13/Image/xd.png')} />
                                
                                <View style={{marginLeft: 8}}>
                                    <Text style={Style.pReg}>Adobe XD</Text>
                                    <Text style={[Style.label, {color: '#9B51E0'}]}>Advanced</Text>
                                </View>
                            </View>

                            <View style={Style.skillData}>
                                <Image source={require('../Tugas13/Image/studio.png')} />
                                
                                <View style={{marginLeft: 8}}>
                                    <Text style={Style.pReg}>Studio</Text>
                                    <Text style={[Style.label, {color: '#9B51E0'}]}>Advanced</Text>
                                </View>
                            </View>
                        </View>

                    </View>
                </SafeAreaView>
            </View>
        )
    }
}