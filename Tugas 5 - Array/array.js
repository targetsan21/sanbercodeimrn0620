function noSoal (nomor) {
    console.log('\n' + '===== Soal Nomor ' + nomor + ' =====' + '\n')
};

// Soal 1
noSoal(1);

let range = (startNum, finishNum) => {
    let array = [];
    if (startNum < finishNum) {
        for (i = startNum; i <= finishNum; i++) {
            array.push(i);
        };
        return array
    } else if (startNum > finishNum) {
        for (i = startNum; i >= finishNum; i--) {
            array.push(i);
        };
        return array
    } else {
        return -1
    }
};

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1  

// Soal 2
noSoal(2);

function rangeWithStep(startNum, finishNum, step) {
    let array = [];
    if (startNum < finishNum) {
        do {
            array.push(startNum);
            startNum += step;
        } while (startNum <= finishNum);
        return array;
    } else {
        do {
            array.push(startNum);
            startNum -= step;
        } while (startNum >= finishNum);
        return array;
    };
};

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// Soal 3
noSoal(3);

function sum (awDer, akhDer, beJar = 1) {
    array = [];
    if (awDer < akhDer) {
        for (i = awDer; i <= akhDer; i += beJar) {
            array.push(i);
        };
        return array.reduce((a, b) => a + b);
    } else if (awDer > akhDer) {
        for (i = awDer; i >= akhDer; i -= beJar) {
            array.push(i);
        };
        return array.reduce((a, b) => a + b);
    } else if (awDer == 1) {
        return 1
    } else if (!awDer) {
        return 0
    }
};

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// Soal 4
noSoal(4);

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(dataArray) {
    dataArray.forEach(function (data) {
        console.log(`Nomor ID: ${data[0]}\nNama Lengkap: ${data[1]}\nTTL: ${data[2]} ${data[3]}\nHobi: ${data[4]}\n`)
    });
};

dataHandling(input);

// Soal 5
noSoal(5);

function balikKata(str) {
    let kata = str;
    let balik = '';

    for (i = str.length-1; i >= 0; i--){
        balik += kata[i];
    }
    return balik;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// Soal 6

noSoal(6);

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(dataArr) {
    dataArr.splice(1, 1, "Roman Alamsyah Elsharawy");
    dataArr.splice(2, 1, "Provinsi Bandar Lampung");
    dataArr.splice(4, 1, "Pria", "SMA International Metro");
    console.log(dataArr);

    let pisah = dataArr[3].split("/") /* misahin "21/05/1989" jadi [ '21', '05', '1989' ] */

    switch(pisah[1]) { /* Ngganti '05' jadi 'Mei' */
        case '05' :
            console.log('Mei');
            break;
    }

    pisah.sort(function (a, b) { /* Sorting dari [ '21', '05', '1989' ] jadi [ '1989', '21', '05' ] */
        return b - a
    });
    console.log(pisah)

    console.log(dataArr[3].split("/").join("-"));

    console.log(dataArr[1].slice(0, 15))
};

dataHandling2(input2);