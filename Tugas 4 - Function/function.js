// Soal 1
function teriak() {
    return 'Halo Sanbers!';
};

console.log(teriak());

// Soal 2
function kalikan(x, y) {
    return x * y;
};

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// Soal 3
function introduce(w, x, y, z) {
    return 'Nama saya ' + w + ', ' + 'umur saya ' + x + ', ' + 'alamat saya di ' + y + ', ' + 'dan saya punya hobby yaitu ' + z;
};

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);