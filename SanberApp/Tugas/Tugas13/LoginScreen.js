import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, TextInput, ScrollView, KeyboardAvoidingView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class App extends Component {
    render() {
        return(
            <KeyboardAvoidingView style={styles.container} behavior='padding'>
                <ScrollView style={{paddingHorizontal: 24, marginBottom: 24, overflow: 'visible'}}>
                    {/* Cubic Logo */}
                    <Image source={require('./Image/logo.png')} style={styles.logoStyle} />
                    
                    {/* Banner */}
                    <View style={styles.welcomeBanner}>
                        <Image source={require('./Image/bubble-bg.png')} style={styles.bannerbg} />

                        <View style={styles.bannerText}>
                            <Text style={styles.heroText}>Welcome!</Text>
                            <Text style={[styles.pTextReg, {color: '#fff'}]}>Sign Up and explore Cubic</Text>
                        </View>
                    </View>

                    {/* Sign Up Button */}
                    <View style={{marginTop: 16}}>
                        {/* Sign Up with Apple */}
                        <TouchableOpacity style={[{backgroundColor: '#000'}, styles.signUpButton]}>
                            <Image source={require('./Image/apple-icon.png')} style={styles.icon}></Image>

                            <Text style={[styles.pReg, {color: '#fff'}]}>Sign Up with Apple</Text>
                        </TouchableOpacity>

                        {/* Sign Up with Google */}
                        <TouchableOpacity style={[{borderWidth: 2, borderColor: '#C9C8C8'}, styles.signUpButton]}>
                            <Image source={require('./Image/google-icon.png')} style={styles.icon}></Image>

                            <Text style={styles.pTextReg}>Sign Up with Google</Text>
                        </TouchableOpacity>
                    </View>

                    {/* Create Account Section */}
                    <View style={styles.createAccSect}>
                        <View style={styles.line}></View>
                        <Text style={[styles.pBold, {color: '#BEBEBE'}]}>OR CREATE AN ACCOUNT</Text>
                        <View style={styles.line}></View>
                    </View>

                    {/* Email */}
                    <View style={styles.inputCont}>
                        <Text style={styles.pMed}>Email</Text>

                        <View style={styles.inputBox}>
                            <TextInput style={styles.input} placeholder='Your Email' placeholderTextColor='#FFC58B' />

                            <Icon name='eye' color='#FFC58B' size={16} />
                        </View>
                    </View>

                    {/* Password */}
                    <View style={styles.inputCont}>
                        <Text style={styles.pMed}>Password</Text>

                        <View style={styles.inputBox}>
                            <TextInput style={styles.input} placeholder='Create Password' placeholderTextColor='#FFC58B' secureTextEntry={true} />

                            <Icon name='eye' color='#FFC58B' size={16} />
                        </View>
                    </View>

                    {/* Confirm Password */}
                    <View style={styles.inputCont}>
                        <Text style={styles.pMed}>Retype Password</Text>

                        <View style={styles.inputBox}>
                            <TextInput style={styles.input} placeholder='Confirm Your Password' placeholderTextColor='#FFC58B'
                            secureTextEntry={true} />

                            <Icon name='eye' color='#FFC58B' size={16} />
                        </View>
                    </View>

                    {/* Submit Button */}
                    <TouchableOpacity style={styles.submitButton}>
                        <LinearGradient 
                            colors = {['#FFCA41', '#D941FF']}
                            start = {[-1, 4]}
                            end = {[1.8, 1]}
                            style = {{width: '100%', height: 56, position: 'absolute'}}
                        />

                        <Text style={[styles.pMed, {color: 'white'}]}>Sign Up</Text>
                    </TouchableOpacity>

                    {/* Go to Log In page */}
                    <View style={{marginTop: 8}}>
                        <Text style={styles.pReg}>Already have an account? <Text style={styles.pLink}>Log In</Text></Text>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
} 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        overflow: 'hidden',
    },

    logoStyle: {
        height: 30, 
        width: 98,
        resizeMode: 'contain', 
        marginTop: 16
    },

    welcomeBanner: {
        borderRadius: 8,
        overflow: 'hidden',
        marginTop: 16,
        width: '100%',
        height: 111,
        justifyContent: 'center',
        // borderColor: 'red',
        // borderWidth: 2
    },

    bannerbg: {
        width: '100%',
        resizeMode: 'contain'
    },

    bannerText: {
        position: 'absolute',
        marginLeft: 16
    },

    heroText: {
        fontSize: 32,
        fontWeight: 'bold',
        color: '#fff',
    },

    pReg: {
        fontSize: 16,
        color: '#545454'
    },

    signUpButton: {
        borderRadius: 8,
        marginVertical: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 12
    },

    icon: {
        width: 24,
        height: 24,
        marginRight: 8
    },

    createAccSect: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 16
    },

    line: {
        width: '16%',
        borderWidth: 1,
        borderColor: '#DEDEDE'
    },

    pBold: {
        fontWeight: 'bold',
        fontSize: 16
    },

    pMed: {
        fontSize: 16,
        fontWeight: '600',
        color: '#545454'
    },

    pLink: {
        color: '#56CCF2',
        textDecorationLine: 'underline'
    },

    inputCont: {
        marginTop: 16
    },

    inputBox: {
        backgroundColor: '#FFF6ED',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 16,
        borderRadius: 8,
        marginTop: 8
    },

    input: {
        fontSize: 16,
        flex: 1,
        paddingHorizontal: 12
    },

    submitButton: {
        marginTop: 16,
        height: 56,
        borderRadius: 8,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center'
    },
})