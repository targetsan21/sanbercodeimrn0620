// Soal 1
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

    // ===== TEST CODE =====

people[0]; // [ 'Bruce', 'Banner', 'male', 1975 ]
people[0][1]; // Banner
people[1][3]; // undefined
people[0].length; // 4

let marvelHeroTest = {}; // Cara nulis Object kosong

marvelHeroTest.firstName = people[0][0];
marvelHeroTest; // { firstName: 'Bruce' } 

    // ===== TEST CODE - END =====

function arrayToObject(variable) {

    if (variable.length == 0) {
        console.log('"');
    } else {
        for (i = 0; i < people.length; i++) {
            let ageCheck;
    
            var now = new Date()
    
            if (variable[i][3] === undefined || variable[i][3] > now.getFullYear()) {
                ageCheck = 'Invalid Birth Year'
            } else {
                ageCheck = now.getFullYear() - variable[i][3]
            }
    
            let marvelHero = {
                firstName : variable[i][0],
                lastName : variable[i][1],
                gender: variable[i][2],
                age: ageCheck
            }
            console.log(`${i + 1} ${variable[i][0]} ${variable[i][1]} : `, marvelHero);
        }
    }
}

arrayToObject(people);
arrayToObject(people2);

/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal 2

    // barang[0] ~> ['Sepatu Stacattu', 1500000]

function shoppingTime(memberId, money) {
    if (!memberId || !money) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    } else {
        let barang = [
            ['Sepatu Stacattu', 1500000], 
            ['Baju Zoro', 500000], 
            ['Baju H&N', 250000], 
            ['Sweater Uniklonoh', 175000], 
            ['Casing Handphone', 50000]
        ];
    
        let listPurchased = [];
        let spendMoney = 0;
    
        for (i = 0; i < barang.length; i++) {
            if (barang[i][1] <= money) {
                listPurchased.push(barang[i][0]);
                spendMoney += barang[i][1]
            }
            money - barang[i][1];
        }
    
        let changeMoney = 0 + (money - spendMoney);
    
        let customer = {
            'memberId' : memberId,
            'money' : money,
            'listPurchased' : listPurchased,
            'changeMoney' : changeMoney
        };
        
        return customer
      }
    }
   
  // TEST CASES

console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }

console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3

// ===== TEST CODE =====

// rute.length ~> 6
// rute.indexOf('A') ~> 0
// rute.splice(rute.indexOf('A'), rute.indexOf('D')) ~> [ 'A', 'B', 'C' ]

// let penumpang = [['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]
// console.log(penumpang[0][1]) ~> B

// ===== TEST CODE - END =====

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    let pickedPass = [];

    if (arrPenumpang.length == 0) {
        return '[]'
    } else {
        for (i = 0; i < 2; i++) {
            let trip = rute.splice(rute.indexOf(arrPenumpang[i][1]), rute.indexOf(arrPenumpang[i][2]) - 1);
            bayar = trip.length * 2000;
    
            if (bayar == 0) {
                let passenger = {
                    'penumpang' : arrPenumpang[i][0],
                    'naikDari' : arrPenumpang[i][1],
                    'tujuan' : arrPenumpang[i][2],
                    'bayar' : 2000
                };
                pickedPass.push(passenger);
            } else {
                let passenger = {
                    'penumpang' : arrPenumpang[i][0],
                    'naikDari' : arrPenumpang[i][1],
                    'tujuan' : arrPenumpang[i][2],
                    'bayar' : bayar
                };
                pickedPass.push(passenger);
            }
        }
    }
    return pickedPass;
}

  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]