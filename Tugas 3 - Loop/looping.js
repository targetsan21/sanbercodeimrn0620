// Soal 1
var itr = 0; /* variable 1 */

    // Looping Pertama
console.log('LOOPING PERTAMA')
while (itr <= 20) {
    itr++;
    if (itr % 2 === 0) {
        console.log (itr, ' - I love coding');
    };
};

itr + 20; /* variable 2 */

    // Looping Kedua
console.log('LOOPING KEDUA')
while (itr >= 2) {
    if (itr % 2 === 0) {
        console.log (itr, ' - I will become a mobile developer');
    };
    itr--;
};

// Soal 2
for (var i = 1; i <= 20; i++) {
    var genap = i%2 === 0;
    if (genap) {
        console.log(i, ' - Berkualitas');
    } else if (!genap && i%3 === 0) {
        console.log(i, ' - I love coding');
    } else {
        console.log(i, ' - Santai');
    }
}

// Soal 3
var x = 8; /* variabel buat nentuin baris */
var y = 4; /* variabel buat nentuin kolom */

var rect = '' /* variabel kosong buat naruh # (STACKOVERFLOW IS MY SAVIOR) */

for (var i = 1; i <= y; i++) { /* loop buat nentuin nasib kolom */
    for (var j = 1; j <= x; j++){ /* loop buat nentuin nasib baris */
        rect = rect.concat('#');
    };
    if (i <= y-1){ /* kondisi biar kalo udah ada 8 # langsung break ke bawah, ada minus (-) nya biar bawahnya ga ikut ke break*/
        rect = rect.concat('\n');
    };
};

console.log(rect); /* eksekusi */

// Soal 4
var y2 = 7; /* variabel buat nentuin kolom */

var stair = ""; /* variabel kosong buat naruh # (STACKOVERFLOW IS MY TRUE SAVIOR!!!) */

for (i = 1; i <= y2; i++) { /* ngeloop 7 kali buat naruh # nya di variabel stair */
    stair = stair.concat('#');
    console.log(stair); /* sumpah ga tau kenapa gini doang udah jadi, magic bet dah mantap */
};

// Soal 5
var x3 = 8; /* baris */
var y3 = 8; /* kolom */

chessBoard = ''; /* variabel kosong */

for (i = 1; i <= y3; i++) { /* nentuin nasib si kolom */
    for (j = 1; j <= x3; j++) { /* nentuin nasib si baris */
        if ((i + j) % 2 == 0) { /* I LOVE YOU STACK OVERFLOW */
            chessBoard = chessBoard.concat(' ');
        } else {
            chessBoard = chessBoard.concat('#');
        }
    };
    if (i <= y3 - 1) { /* kondisi biar ada breaknya kalo kolomnya udah sampe 8. Tapi bawahnya jangan sampe ke break juga */
        chessBoard = chessBoard.concat('\n');
    }
};

console.log(chessBoard); /* dah kelar */