import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// Tugas 12
import YoutubeUI from './Tugas/Tugas12/app';

// Import Index.js
import Idx from './Latihan/hari-3-styling-flexbox'

// Tugas 13
import Register from './Tugas/Tugas13/LoginScreen' /* Register Page */
import AboutMe from './Tugas/Tugas13/AboutScreen' /* About Page */

// Tugas 14
import ToDoApp from './Tugas/Tugas14/App'
import SkillScreen from './Tugas/Tugas14/SkillScreen'

// Day 5 React Native Latihan React Navigation
import ReactNav from './Latihan/hari-5-react-navigation/index'

// Tugas 15
import ReactNavTut from './Tugas/Tugas15/index'

// Tugas Navigation
import TugasNav from './Tugas/TugasNavigation/index'

// Quiz 3
import LogIn from './Quiz3/LoginScreen'

export default function App() {
  return(
    <View style={styles.container}>
      {/* <YoutubeUI/> */}

      {/* <Idx /> */}

      {/* <Register /> */}

      {/* <AboutMe /> */}

      {/* <ToDoApp /> */}

      {/* <SkillScreen /> */}

      {/* <ReactNav /> */}

      {/* <ReactNavTut /> */}

      {/* <TugasNav /> */}

      <LogIn />

      <StatusBar style='black' />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});